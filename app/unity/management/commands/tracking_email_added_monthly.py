from django.core.management.base import BaseCommand
from unity.models import Users
from datetime import datetime


class Command(BaseCommand):
    help = 'Tracking monthly new email added option --date '

    def add_arguments(self, parser):
        parser.add_argument(
            '--date',
            help='Tracking monthly new email added with date',
            required=False
        )

    def handle(self, *args, **options):
        month = datetime.strftime(datetime.now(), "%m")
        year = datetime.strftime(datetime.now(), "%Y")

        if options['date']:
            year = datetime.strftime(datetime.strptime(
                options['date'], '%Y-%m-%d'), '%Y')
            month = datetime.strftime(datetime.strptime(
                options['date'], '%Y-%m-%d'), '%m')

        user_subscribe_on_month = Users.objects.filter(
            created_at__year=year, created_at__month=month).count()

        self.stdout.write(self.style.SUCCESS(
            'New monthly email subscribe "%s"' % user_subscribe_on_month))
