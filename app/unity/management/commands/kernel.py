from crontab import CronTab
import os

PROJECT_PATH = os.path.abspath(os.path.dirname(__name__))
with CronTab(user='macbook') as cron:
    job = cron.new(command=f'cd {PROJECT_PATH} && python3 manage.py tracking_email_added_monthly')
    job.day.on(2,4)
    cron.write()
print('Cronjob successfully on 2, 4 every week ! check it using crontab -l')
    