from django.urls import path
from .api.users import ListCreateUserView, UserActionOnStore

urlpatterns = [
    path('users', ListCreateUserView.as_view(), name='index'),
    path('users/<int:pk>/unsubscribe', UserActionOnStore.as_view()),
]
