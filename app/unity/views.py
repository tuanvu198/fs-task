from app.response import response_ok

def index(request):
    return response_ok("Hello, world. You're at the polls index.")