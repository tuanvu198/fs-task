from app.response import response_created, response_validation, response_ok
from unity.models import Users, StatusUserEnum
from unity.serializers import UserSerializer
from rest_framework.generics import CreateAPIView, UpdateAPIView, get_object_or_404
from rest_framework.views import APIView
from datetime import datetime


class ListCreateUserView(CreateAPIView, APIView):
    def get(self, request, format=None):
        month = datetime.strftime(datetime.now(), "%m")
        year = datetime.strftime(datetime.now(), "%Y")

        user_subscribe_on_month = Users.objects.filter(deleted_at__isnull=True, status=StatusUserEnum.UNSUBSCRIBE.value,
                                                       created_at__year=year, created_at__month=month).count()
        user_unsubscribe_on_month = Users.objects.filter(
            deleted_at__isnull=True, status=StatusUserEnum.UNSUBSCRIBE.value, deleted_at__year=year, deleted_at__month=month).count()

        obj = Users.objects.filter(deleted_at__isnull=True).all()

        users = [user.serialize for user in obj]

        res = {
            'user_subscribe_on_month': user_subscribe_on_month,
            'user_unsubscribe_on_month': user_unsubscribe_on_month,
            'total': obj.count(),
            'users': users,
        }
        return response_ok(res)

    def create(self, request, *args, **kwargs):
        serializer = UserSerializer(data=request.data)

        if serializer.is_valid(raise_exception=True):
            serializer.save()
            return response_created(serializer.data)

        return response_validation(serializer.error_messages)


class UserActionOnStore(UpdateAPIView):
    def update(self, request, pk, *args, **kwargs):
        get_object_or_404(Users, id=pk)

        Users.objects.filter(pk=pk).update(
            status=StatusUserEnum.UNSUBSCRIBE.value)

        return response_ok(Users.objects.get(pk=pk).serialize)
