from enum import Enum
from django.db import models

# Create your models here.

class Users(models.Model):
    email = models.EmailField(max_length=100)
    status = models.SmallIntegerField(default=1)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)
    deleted_at = models.DateTimeField(null=True)

    def __str__(self) -> str:
        return self.email
    
    @property
    def serialize(self):
        data = {
            'id': self.id,
            'email': self.email,
            'status': self.status,
            'created_at': self.created_at.isoformat(sep=' ', timespec='seconds'),
            'updated_at': self.updated_at.isoformat(sep=' ', timespec='seconds'),
        }
        
        return data

class StatusUserEnum(Enum):
    UNSUBSCRIBE = 0
    SUBSCRIBE = 1