from rest_framework.pagination import PageNumberPagination

from app.response import response_ok


class Pagination(PageNumberPagination):
    page_size = 2
    page_size_query_param = 'page_size'
    max_page_size = 10000

    def get_paginated_response(self, data):
        return response_ok(data, meta={
            'links': {
                'next': self.get_next_link(),
                'previous': self.get_previous_link(),
                'page_size': self.page_size
            },
            'count': self.page.paginator.count,
        })